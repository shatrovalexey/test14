<?php
	/**
	* Скрипт для подключения обработчиков
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	*/

	/**
	* @include - подключение файла инициализации
	*/
	include_once( '../../bin/init.php' ) ;

	ini_set( 'display_errors' , E_ALL ) ;
	$app->execute( @$_REQUEST[ 'mode' ] ) ;