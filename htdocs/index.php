<?php
	ini_set( 'display_errors' , E_ALL ) ;

	include_once( '../vendor/autoload.php' ) ;

	$app = new Application\Settings( ) ;
?><!DOCTYPE html>

<html>
	<head>
		<title><?=htmlspecialchars( $app->config->apptitle )?></title>
		<meta charset="<?=htmlspecialchars( $app->config->charset )?>">
		<link rel="stylesheet" type="text/css" href="js/ext-6.5.1/build/resources/css/ext-all.css" />
		<script type="text/javascript" src="js/ext-6.5.1/ext-bootstrap.js"></script>
		<script>
Ext.define( "Logs" , {
	"extend" : "Ext.data.Store" ,
	"alias" : "store.logs" ,
	"proxy" : {
		"type" : "ajax" ,
		"api" : {
			"read" : "ctrl/?mode=datasource"
		} ,
		"reader" : {
			"type" : "json" ,
			"totalProperty" : "totalProperty"
		}
	} ,
	"buffered" : true ,
	"pageSize" : <?=json_encode( $app->config->record_limit )?>
} ) ;

Ext.application( {
	"name" : <?=json_encode( $app->config->apptitle )?> ,
	"launch" : function( ) {
		Ext.Viewport.add( {
			"xtype" : "tabpanel" ,
			"controller" : "listview" ,
			"plugins" : [ "gridfilters" ] ,
			"items" : [ {
				"title" : <?=json_encode( $app->config->subtitle )?> ,
				"xtype" : "grid" ,
				"store" : {
					"type" : "logs" ,
					"autoLoad" : true ,
					"sorters" : <?=json_encode( array_map( function( $item ) {
						return $item->name ;
					} , $app->config->result->metaData->fields ) )?>
				} ,
				"columns" : <?=json_encode( $app->config->result->metaData->columns )?>
			} ]
		} ) ;
	}
} ) ;
		</script>
	</head>
	<body></body>
</html>