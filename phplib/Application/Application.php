<?php
	/**
	* Базовый класс
	*
	* Родительский класс для других классов
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application
	*/

	namespace Application ;

	/**
	* Базовый класс
	* @package Application
	* @subpackage Application
	*/
	class Application extends \stdClass {
		/**
		* Запуск примера выполнения класса
		* @param $mode string - возможные режимы запуска:
		* * generate - генерация данных журналов
		* * upload - загрузка данных сгенерированных журналов
		* * datasource - выполнение HTTP-запроса от ExtJS Grid
		* @return mixed
		*/
		public function execute( $mode ) {
			$mode = ucfirst( $mode ) ;

			/**
			* Возврат null, если указан недопустимый режим запуска
			*/
			if ( ! in_array( $mode , array(
				'Generate' , 'Upload' , 'Datasource'
			) ) ) {
				error_log( 'Mode ' . $mode . ' is undefined' ) ;

				return null ;
			}

			/**
			* @var $class string - имя класса для создания объекта
			* @var $app stdClass - объект класса $class
			* @var $result mixed - результат выполнения $app->execute
			* выход из блока, если во время выполнения произошла ошибка
			*/
			try {
				$class = "Application\\{$mode}" ;
				$app = new $class( ) ;
				$result = $app->execute( ) ;

				return $result ;
			} catch( Exception $exception ) {
				error_log( $exception->getMessage( ) ) ;

				return null ;
			}
		}
	}