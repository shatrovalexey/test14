<?php
	/**
	* Класс для обработки HTTP-запросов от ExtJS Grid
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\Datasource
	*/

	namespace Application ;

	class Datasource extends DBA {
		/**
		* Выполнение обработки HTTP-запроса
		* @return mixed - результат выполнения
		*/
		function execute( ) {
			/**
			* @var $request mixed - HTTP-аргументы запроса
			*/
			$request = &$_REQUEST ;

			/**
			* @var $page_current int - текущая страница данных
			*/
			if ( empty( $request[ 'page' ] ) ) {
				$page_current = 0 ;
			} else {
				$page_current = intval( $request[ 'page' ] ) ;
			}

			/**
			* @var $page_size int - размер страницы данных
			*/
			if ( empty( $request[ 'limit' ] ) ) {
				$page_size = $this->config->record_limit ;
			} elseif ( is_numeric( $request[ 'limit' ] ) ) {
				$page_size = intval( $request[ 'limit' ] ) ;
			} else {
				$page_size = $this->config->record_limit ;
			}

			/**
			* @var $order_dir string - направление упорядочения данных
			*/
			if ( empty( $request[ 'dir' ] ) ) {
				$order_dir = null ;
			} elseif ( in_array( $request[ 'dir' ] , array( 'ASC' , 'DESC' ) ) ) {
				$order_dir = $request[ 'dir' ] ;
			} else {
				$order_dir = null ;
			}

			/**
			* @var $result mixed - шаблон структуры данных для ExtJS
			*/
			$result = clone $this->config->result ;

			/**
			* @var $order_by string - столбец упорядочения данных
			*/
			if ( empty( $request[ 'sort' ] ) ) {
				$order_by = null ;
			} elseif ( array_filter( $result->metaData->fields , function( $item ) use( &$request ) {
				return $item->name == $request[ 'sort' ]  ;
			} ) ) {
				$order_by = $request[ 'sort' ] ;
			} else {
				$order_by = null ;
			}

			/**
			* @var $record_current int - текущая запись
			*/
			if ( empty( $request[ 'start' ] ) ) {
				$record_current = 0 ;
			} elseif ( is_numeric( $request[ 'start' ] ) ) {
				$record_current = intval( $request[ 'start' ] ) ;
			} else {
				$record_current = 0 ;
			}

			/**
			* @var $sql_code array - список строк, из которых будет составлен SQL-запрос для выполнения
			*/
			$sql_code = array( 'SELECT' , 'count( * ) AS "count"' , 'FROM "v_log" AS "vl1"' ) ;

			/**
			* @var $sth PDOStatement - подготовленный SQL-запрос
			* @var $result->totalCount int - общее количество записей
			*/
			$sth = $this->dba( )->prepare( implode( PHP_EOL , $sql_code ) ) ;
			$sth->execute( ) ;
			list( $result->totalCount ) = $sth->fetch( \PDO::FETCH_NUM ) ;

			/**
			* изменение результатов вывода
			*/
			$sql_code[ 1 ] = '"vl1".*' ;

			/**
			* определение предпочтений упорядочения данных
			*/
			if ( ! empty( $order_by ) ) {
				$sql_code[] = '
ORDER BY
	"vl1"."' . $order_by . '"
				' ;

				if ( ! empty( $order_dir ) ) {
					$sql_code[] = $order_dir ;
				}
			}

			/**
			* указание страницы данных
			*/
			$sql_code[] = '
OFFSET :offset LIMIT :limit
			' ;

			$sth = $this->dba( )->prepare( implode( PHP_EOL , $sql_code ) ) ;
			$sth->bindValue( ':offset' , $page_current * $page_size , \PDO::PARAM_INT ) ;
			$sth->bindValue( ':limit' , $page_size , \PDO::PARAM_INT ) ;
			$sth->execute( ) ;

			/**
			* @var $result->rows mixed - все данные страницы
			*/
			$result->rows = $sth->fetchAll( \PDO::FETCH_ASSOC ) ;

			/**
			* публикация данных
			*/
			return $this->publish( $result , array(
				$this->config->http->ctype . ': ' . $this->config->http->json . '; charset=' . $this->config->charset
			) ) ;
		}

		/**
		* Публикация данных
		* @param $body string | mixed - данные тела ответа. Если данные представлены в виде объекта, то они кодируются в json
		* @param $headers array - список HTTP-заголовков
		*/
		function publish( $body = null , $headers = array( ) ) {
			/**
			* вывод HTTP-заголовков
			*/
			if ( ! empty( $headers ) && ! headers_sent( ) ) {
				foreach ( $headers as $i => $header ) {
					header( $header ) ;
				}
			}

			/**
			* вывод тела ответа
			*/
			if ( ! empty( $body ) && ! is_string( $body ) ) {
				$body = @json_encode( $body ) ;
			}

			echo $body ;
		}
	}