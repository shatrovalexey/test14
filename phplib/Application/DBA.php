<?php
	/**
	* Класс для подключения к СУБД
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\DBA
	*/
	namespace Application ;

	class DBA extends Settings {
		/**
		* @var $dbh resourse - подключение к СУБД
		*/
		var $dbh ;

		/**
		* Подключение к СУБД
		* Переподключение, если подключение было создано и неактивно. Или новое подключение.
		* @return resourse - подключение к СУБД
		*/
		function dba( ) {
			while ( true ) {
				if ( empty( $this->dbh ) ) {
					try {
						$this->dbh = new \PDO(
							$this->config->dba->connection_string_dbo ,
							$this->config->dba->login ,
							$this->config->dba->password
						) ;
						$this->dbh->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION ) ;
					} catch ( Exception $exception ) {
						die( $exception->getMessage( ) ) ;
					}
					continue ;
				}

				try {
					$this->dbh->query( 'SELECT 1' ) ;

					break ;
				} catch ( Exception $exception ) {
					$this->dbh = null ;
				}
			}

			return $this->dbh ;
		}
	}