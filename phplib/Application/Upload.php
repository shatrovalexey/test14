<?php
	/**
	* Класс для загрузки данных в БД из файлов журналов
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\Upload
	*/

	namespace Application ;

	class Upload extends DBA {
		/**
		* Выполнение загрузки данных
		* @return boolean
		*/
		public function execute( ) {
			/**
			* Выполнение скриптов перед загрузкой данных
			*/
			foreach ( $this->config->upload->script as $i => &$query ) {
				$this->dba( )->query( $query ) ;
			}

			/**
			* Выполнение загрузки данных из файлов журналов
			*/
			foreach ( $this->config->upload->copy as $i => &$item ) {
				$this->dba( )->pgsqlCopyFromFile(
					$item->table_name ,
					$this->config->log[ $item->log_id ] ,
					$this->config->csv->sep_char ,
					null ,
					$item->fields
				) ;
			}

			return true ;
		}
	}