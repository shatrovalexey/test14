<?php
	/**
	* Общий класс с общими методами для прочих классов
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\Settings
	*/

	namespace Application ;

	class Settings extends Application {
		/**
		* @const CONFIG_FILE_NAME string - путь к файлу настроек по-умолчанию
		*/
		const CONFIG_FILE_NAME = 'config/config.json' ;

		/**
		* Конструктор
		*/
		function __construct( ) {
			/**
			* Загрузка файла настроек
			*/
			$this->configure( ) ;
		}

		/**
		* Создание файлового дескриптора, связанного с путём к файлу
		* @param $file_name string - путь к файлу
		* @param $mode string - режим доступа к файлу
		* @return resourse - файловый дескриптор
		*/
		function mkfh( $file_name , $mode = 'rb' ) {
			/**
			* @var $fh resourse - файловый дескриптор
			*/
			$fh = fopen( $file_name , $mode ) ;

			/**
			* выход из процесса, если создание дескриптора не удалось
			*/
			if ( empty( $fh ) ) {
				exit( 0 ) ;
			}

			return $fh ;
		}

		/**
		* Возврат свойства $this->$key или заполнение $sub( ), если оно пусто
		* @param $key string - имя свойства
		* @param $sub function - функция для заполнения свойства $key
		* @return mixed - значение свойства $this->$key
		*/
		protected function __coalesce( $key , $sub ) {
			if ( ! empty( $this->$key ) ) {
				return $this->$key ;
			}

			$this->$key = $sub( ) ;

			return $this->$key ;
		}

		/**
		* Загрузка настроек
		* @param $file_name string - имя файла настроек
		* @return mixed - загруженные данные настроек
		*/
		protected function __configure( $file_name = self::CONFIG_FILE_NAME ) {
			/**
			* @var $data string - строка с настройками из файла $file_name
			*/

			$file_name = __DIR__ . '/../../' . $file_name ;

			$data = file_get_contents( $file_name ) ;
			if ( empty( $data ) ) {
				exit( 0 ) ;
			}

			/**
			* @var $result mixed - структура данных с настройками
			*/
			$result = json_decode( $data ) ;
			if ( empty( $result ) ) {
				exit( 0 ) ;
			}

			return $result ;
		}

		/**
		* Загрузка настроек в $this
		* @param $file_name string - имя файла настроек
		* @return mixed - загруженные данные настроек
		*/
		function configure( $file_name = self::CONFIG_FILE_NAME ) {
			$result = $this->__coalesce( 'config' , function( ) use( &$file_name ) {
				return $this->__configure( $file_name ) ;
			} ) ;

			return $result ;
		}

		/**
		* Выполнение функции с перехватом исключений
		* @param $sub function - функция для выполнения
		* @param $args array - список аргументов для выполнения $sub
		* @return mixed - результат выполнения функции
		*/
		protected function __eval( $sub , $args = array( ) ) {
			try {
				return call_user_func_array( $sub , $args ) ;
			} catch ( Exception $exception ) {
				trigger_error( $exception->getMessage( ) ) ;
			}

			return null ;
		}
	}