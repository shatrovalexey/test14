<?php
	/**
	* Класс для генерации файлов журналов
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\Generate
	*/

	namespace Application ;

	class Generate extends DBA {
		/**
		* Настройка загрузки файла настроек
		* @param $file_name string - путь к файлу настроек
		* @return mixed - структура данных с настройками
		*/
		function configure( $file_name = self::CONFIG_FILE_NAME ) {
			$config = parent::configure( $file_name ) ;
			$generate = parent::__configure( $this->config->generate_file_name ) ;

			foreach ( $generate as $key => $value ) {
				$config->$key = $generate->$key ;
			}

			return $config ;
		}

		/**
		* Односторонне шифрование строки
		* @param $data string - строка для шифрования. Если аргумент не передан, то используется произвольное значение.
		* @return string( 32 ) - MD5-хэш от $data
		*/
		function digest( $data = null ) {
			if ( ! func_num_args( ) ) {
				$data = uniqid( ) ;
			}

			return md5( $data ) ;
		}

		/**
		* Генерация IPv4
		* @return string - IPv4
		*/
		function ipv4( ) {
			$ipv4 = array( ) ;

			for ( $i = 0 ; $i < 4 ; $i ++ ) {
				$ipv4[] = intval( rand( 0 , $this->config->ip_range ) ) ;
			}

			return implode( '.' , $ipv4 ) ;
		}

		/**
		* Генерация User-Agent
		* @return string - User-Agent
		*/
		function user_agent( ) {
			return $this->config->user_agent[ array_rand( $this->config->user_agent ) ] ;
		}

		/**
		* Генерация названия ОС
		* @return string - название ОС
		*/
		function os_name( ) {
			return $this->config->os_name[ array_rand( $this->config->os_name ) ] ;
		}

		/**
		* Генерация даты\времени
		* @return string - дата\время в заданном настройками формате
		*/
		function datetime( $position = 0 ) {
			$time = time( ) - $this->config->datetime_shift + $position ;

			return strftime( $this->config->datetime_format , $time ) ;
		}

		/**
		* Генерация URI
		* @return string - URI в заданном настройками формате
		*/
		function uri( ) {
			return sprintf( $this->config->url_format , $this->digest( ) ) ;
		}

		/**
		* Генерация журналов
		* @return boolean - true, если успешно
		*/
		function execute( ) {
			/**
			* @var $log_fh array - список файловых дескрипторов журналов
			*/
			$log_fh = array( ) ;

			foreach ( $this->config->log as $i => $file_name ) {
				$log_fh[] = $this->mkfh( $file_name , 'wb' ) ;
			}

			/**
			* @var $ipv4_arr array - хэш-массив сгенерированных IPv4-адресов для проверки уникальности
			*/
			$ipv4_arr = array( ) ;

			/**
			* заполнение файлов журналов
			*/
			for ( $i = $this->config->ip_count ; $i >= 0 ; $i -- ) {
				/**
				* @var $ipv4 string - сгенерированный IPv4-адрес
				*/
				$ipv4 = $this->ipv4( ) ;

				/**
				* повтор генерации, если IPv4 сгенерирован повторно
				*/
				if ( isset( $ipv4_arr[ $ipv4 ] ) ) {
					$i ++ ;

					continue ;
				}

				$ipv4_arr[ $ipv4 ] = true ;

				/**
				* заполнение файла-справочника клиентов
				*/
				fputcsv( $log_fh[ 1 ] , array(
					$ipv4 , $this->user_agent( ) , $this->os_name( )
				) , $this->config->csv->sep_char ) ;

				/**
				* заполнение файла журнала
				*/
				for ( $j = $this->config->ip_log_count ; $j >= 0 ; $j -- ) {
					fputcsv( $log_fh[ 0 ] , array(
						$this->datetime( $j ) , $ipv4 , $this->uri( ) , $this->uri( )
					) , $this->config->csv->sep_char ) ;
				}
			}

			/**
			* закрытие файловых дескрипторов
			*/
			foreach ( $log_fh as $i => &$fh ) {
				fclose( $fh ) ;
			}

			return true ;
		}
	}
