<?php
	/**
	* Файл инициализации для запуска примеров
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	*/

	/**
	* @include - подключение файла для автозагрузки классов
	*/
	require_once( __DIR__ . '/../vendor/autoload.php' ) ;

	/**
	* @var $app stdClass - объект приложения
	*/
	$app = new \Application\Application( ) ;