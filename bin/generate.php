<?php
	/**
	* Скрипт для запуска генерации файлов журналов
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	*/

	/**
	* @include - подключение файла инициализации
	*/
	require_once( 'init.php' ) ;

	$app->execute( 'generate' ) ;