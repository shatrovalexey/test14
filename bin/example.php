<?php
	/**
	* Скрипт для запуска примеров
	* Запуск:
	* `php example.php generate` - генерация журналов
	* `php example.php upload` - загрузка данных журналов в БД
	* `php example.php datasource` - обработка HTTP-запросов от ExtJS Grid
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	*/

	/**
	* @include - подключение файла инициализации
	*/
	include_once( 'init.php' ) ;

	$app->execute( $argv[ 1 ] ) ;