<?php
	/**
	* Файл инициализации для запуска примеров
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	*/

	chdir( '../htdocs' ) ;
	`php -S localhost:8001` ;